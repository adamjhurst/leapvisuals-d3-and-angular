'use strict';

/* Services */



angular.module('myApp.services', [])
	.value('version', '0.1')
	.service('d3Graphics', function() {

		var that = this;

		this.SvgBase = (function(){
			var _svg = {};
			_svg.settings = {
				width:window.innerWidth,
				height:window.innerHeight-190,
				divTarget: "#svg-container",
				margin:10
			};
			_svg.svg = {};
			_svg.resizeSVG = function(){
				settings.width = window.innerWidth-60;
				settings.height = window.innerHeight-60;

				_svg.svg
					.attr("width",_svg.settings.width-_svg.settings.margin)
					.attr("height", _svg.settings.height-_svg.settings.margin);
				};
			_svg.destroy = function(){ _svg.svg.remove() };
			_svg.build = function(){
				console.debug("Building SVG:\t"+_svg.settings.divTarget)
				_svg.svg = d3.select(_svg.settings.divTarget)
					.append("svg")
					.attr("width", _svg.settings.width)
					.attr("height", _svg.settings.height);
				return _svg.svg
			};

			return _svg;
		}());

		this.colours = {
			darkblue:"0,38,53",
			lightblue:"1,52,64",
			red:"171,26,37",
			yellow:"217,121,37"
		};

		this.g = {};
		this.initiate = function(id){ that.g = that.SvgBase.svg.append("g").attr("id",id)};


        	function BaseGraphicsObject(){
        		this.l = [];
        		this.x = 0;
        		this.y = 0;
        		//var _t = this;

        		this.show = function(){this.g.style("display","block"); return this}
        		this.hide = function(){this.g.style("display","none"); return this}
        		this.remove = function(){this.g.remove()}
        		this.select  = function(){ return this.obj}; // Why bother?
        		this.move = function(x,y){
        			this.x = x;
        			this.y = y;
        			this.g.attr("transform","translate("+x+","+y+")");
        			return this;
        			};
        		this.offset = function(x,y){
        			this.gOffset.attr("transform","translate("+x+","+y+")")
        			return this
        			};
        		this.drag = function(){
        			var _t = this;
        			var drag = d3.behavior.drag().on("drag", function(){
        				_t.x += d3.event.dx;
        				_t.y += d3.event.dy;
        				_t.l.forEach(function(f){f(_t.x,_t.y)});
        				_t.g.attr("transform","translate("+_t.x+","+_t.y+")")
        				});
        			this.g.call(drag);
        			return this;
        		};
        		this.hook = function(func){
        			if(typeof(func)=='function'){ this.l.push(func); };
        			return this;
        		};
        	};

        	this.tip = function(x,y,text,g){
                // http://bl.ocks.org/mbostock/6123708
        		//var d = [{x:x,y:y}];
        		var tTemp = g || that.g;
                var g = tTemp.append("g").attr("id","tip").attr("transform","translate("+x+","+y+")");
                var width = function(val){
        			if(typeof(val)=="number"){
        				var len = Math.floor((Math.log(val)/Math.log(10)+1));
        				return len*11+9
        			}
        			else if(typeof(val)=="string"){
        				var len = val.length;
        				console.debug(len*10+8)
        				return len*10+8
        			};
        			return len*11+6};
                var d = {x:x,y:y,w:width(text)};

                var rect = g.append("rect")
                    .attr("rx", 3)
                    .attr("ry", 3)
                    .attr("x", 0)
                    .attr("y", 0)
                    .attr("width", 30) //d.w) // Function of the number of characters
                    .attr("height", 26)
                    .attr("fill","steelblue")
                    .attr("stroke","steelblue")
                    .attr("stroke-width",2);

                var t = g.append("text")
                    .attr("x",6)
                    .attr("y",18)
                    .style("font-size","18px")
                    .text(text)
                    .attr("fill","white")
                    //.attr("fill","steelblue");

                this.move  = function(x,y){ g.attr("transform", "translate(" + (x) + ", " + (y) + ")"); return this;};
                this.text  = function(val){
                    d.w = width(val);
        			//console.debug("val \t"+val+"\nwidth "+length);
                    t.text(val);
                    rect.attr("width",d.w);
                    return this;};
                this.hide  = function(){ g.style("display","none"); return this;};
                this.show  = function(){ g.style("display","block"); return this;};
                this.select  = function(){ return g;};
                this.remove  = function(){ g.remove(); };
            };

        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.rect = function(xVal,yVal,w,h,gInput){
        		var _t = this;
        		this.x=xVal;
        		this.y=yVal;
        		this.l = [];
        		this.w = w;
        		this.gOffset =
        		this.g = gInput || that.g.append("g")
        			.attr("id","rect")
        			.attr("transform", "translate(" +this.x+ ", " +this.y+ ")");
        		var rect = this.g.append("rect")
        			.attr("rx", 3)
        			.attr("ry", 3)
        			.attr("x", 0)
        			.attr("y", 0)
        			.attr("width", this.w)
        			.attr("height", h)
        			.attr("fill","steelblue");
        		var text = this.g.append("text")
        			.attr("x",-7)
        			.attr("y",7)
        			.style("font-size","18px")
        			.style("fill","steelblue")
        			.style("display","none");
        		this.text  = function(input){text.text(input).style("display","block"); return this};
        		this.color  = function(color){ rect.attr("fill",color);return this};
        	}
        	this.rect.prototype = new BaseGraphicsObject();

        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.circ = function(xVal,yVal,r){
        		this.g = that.g.append("g").attr("id","circle");
        		//var data = [{"x":x,"y":y,"r":r}];
        		var x=xVal,y=yVal;
        		var text = false;
        		var l = [];
        		var obj = this.g
        			.append("circle")
        			.attr("class","circle")
        			.attr("cx", x )
        			.attr("cy", y )
        			.attr("r",r )
        			.attr("fill", "white")
        			.attr("stroke-width", 3)
        			.attr("stroke", "steelblue")
        			.on("mouseover",function(){
        				d3.select(this).transition().duration(500).attr("stroke-width", 6)
        			})
        			.on("mouseout",function(){
        				d3.select(this).transition().duration(200).attr("stroke-width", 3)
        			});

        		this.move  = function(x,y){
        			x = xVal;
        			y = yVal;
        			obj.attr("cx", x).attr("cy",y);
        			return this; };
        		this.text  = function(val){
        			if(text==false){
        				text = this.g.append("text")
        					.attr("x",x-5)
        					.attr("y",y+6)
        					.style("font-size","18px")
        					.style("fill","steelblue")
        					.text(val)
        					.on("mouseover",function(){
        						obj.transition().duration(500).attr("stroke-width", 6)
        					})
        					.on("mouseout",function(){
        						obj.transition().duration(200).attr("stroke-width", 3)
        					});
        			}
        			else{
        				text.text(val)
        			};
        			return this;};
        		this.hook  = function(func){
        			if(typeof(func)=='function'){ l.push(func); };
        			return this;
        			};
        		this.drag  = function(hook,start,end){

                    if(typeof(hook)=='undefined'){
                        var drag = d3.behavior.drag().on("drag", function(d){
        					x += d3.event.dx;
        					y += d3.event.dy;
        					l.forEach(function(f){f(x,y)});
        					obj.attr("cx", x)
        					   .attr("cy", y);
        					if(typeof(text)=="object"){
        						text.attr("x",x-5)
        							.attr("y",y+6)
        						};
        					});
                    }
                    else if(typeof(hook)=='function'){
        				console.debug(l);
        				var drag = d3.behavior.drag().on("drag", function(d){
        					var x = d3.event.x;
        					var y = d3.event.y;
        					hook(x,y);
        					l.forEach(function(f){f(x,y)});
        					obj.attr("cx", d.x = x).attr("cy", d.y = y);
        					})
        					console.debug(typeof(text));
        					if(text!=false){
        						text.attr("x",x+4)
        							.attr("y",y+7)
        					}
        				}
                    if(typeof(start)=='function'){drag.on("dragstart",start)};
        			if(typeof(end)=='function'){drag.on("dragend",end)};
                    obj.call(drag);
                    return this;};

        		this.select  = function(){ return obj;};
        		return this
        	};
        	this.circ.prototype = new BaseGraphicsObject();
        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.line = function(x1,y1,x2,y2,w){
        		var width = w || 4;
        		this.g = that.g.append("g").attr("id","line");
        		var gl = this.g.append("g").attr("id","line");
        		var gt = this.g.append("g").attr("id","toolTip");
        		var gx1=x1,gy1=y1, gx2=x2,gy2=y2, tipEnabled = false;
        		var hTip = 20, wTip = 20; // Height and width of toolthip
        		var xTip,yTip;

        		var obj = gl.append("line")
        			.attr("x1", x1)
        			.attr("y1", y1)
        			.attr("x2", x2)
        			.attr("y2", y2)
        			.attr("stroke-width", width)
        			.attr("stroke","steelblue");
        		var tip;

        		this.tipText = function(val){tip.text(val); return this;};
        		var tipSet = function(){
        			w = gx2-gx1;
        			h = gy2-gy1;
        			xTip = gx1+(w)/2;
        			yTip = gy1+(h)/2;
        			tip.select().attr("transform","translate("+xTip+","+yTip+")");
        			var angle = Math.round(Math.atan(h/w)*180/Math.PI);
        			gt.attr("transform"," rotate("+angle+","+xTip+","+yTip+") translate("+(-wTip/2)+","+(-hTip/2)+")");
        		};

        		this.enableTip  = function(){
        			tipEnabled = true;
        			tip = new that.tip(10,10,"Hello",gt);
        			tipSet();
        			return this;};

        		this.start  = function(x,y){ if(tipEnabled){ gx1=x; gy1=y; tipSet(); }; obj.attr("x1", x).attr("y1", y); return this};
        		this.end  = function(x,y){ if(tipEnabled){ gx2=x; gy2=y; tipSet(); }; obj.attr("x2", x).attr("y2", y); return this};
        		this.move  = function(x,y){
        			if(tipEnabled){ gx1=x; gy1=y; tipSet(); }; // Problem
        			obj.attr("transform", "translate(" + (x) + ", " + (y) + ")"); return this;};
        		this.text  = function(){
        			text = new that.circ(0,0,10);
        			return this;};
        		this.setText  = function(x,y){
        			text.move(x,y);
        			return this;};
        		return this
        	};
        	this.line.prototype = new BaseGraphicsObject();
        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.arc = function(x,y,radius,start,end,group){
        		this.g = group || that.g.append("g").attr("id","arc");
        		var radians = 2 * Math.PI,  points = 50;
        		this.data = {x:x,y:y,r:radius,start:start,end:end};

        		var angle = d3.scale.linear()
        			.domain([0, points-1])
        			.range([start*radians/360, end*radians/360]);

        		var line = d3.svg.line.radial()
        			.interpolate("basis")
        			.tension(0)
        			.radius(radius)
        			.angle(function(d, i) { return angle(i); });

        		this.obj = this.g.append("path").datum(d3.range(points))
        			.attr("class", "line")
        			.attr("d", line)
        			.attr("fill-opacity",0)
        			.attr("stroke","steelblue")
        			.attr("stroke-width",2)
        			.attr("stroke-opacity",0.9)
        			.attr("transform", "translate(" + (x) + ", " + (y) + ")");

        		this.rotate  = function(angle){  // NOT WORKING??
        			console.debug(this.data)
        			this.data.start += angle;
        			this.data.end += angle; // This part works
        			angle = d3.scale.linear().domain([0, points-1]).range([this.data.start*radians/360, this.data.end*radians/360]);
        			this.obj.attr("d", line);
        			console.debug(this.data)
        			return this;
        		};
        		this.angle  = function(start,end){
        			this.data.start = start; this.data.end = end;
        			angle = d3.scale.linear().domain([0, points-1]).range([this.data.start*radians/360, this.data.end*radians/360]);
        			this.obj.attr("d", line);
        			return this};
        		this.pop  = function(){ line.radius(this.data.r); this.obj.transition().duration(500).attr("d", line);  return this;};
        		this.unpop  = function(){ line.radius(0); this.obj.transition().duration(500).attr("d", line);return this;};
        		return this;
        	};
        	this.arc.prototype = new BaseGraphicsObject();
        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.toolTip = function(xVal,yVal,width,radius,group){
        		this.x=xVal;
        		this.y=yVal;
        		var w = width || 200;
        		var h=60;
        		this.l = [];

        		var r = radius || 11;
        		var charMax = Math.round(w / 6);
        		var fontSize = 16;
        		var lineSpacing = 10;

        		var hide = function(){
        			title.style("display","none");
        			desc.style("display","none");
        			rect.transition().duration(500).attr("height",10);
        			rect.transition().delay(500).duration(500).attr("width",10);
        			//gTip.transition().duration(900).attr("opacity",0);
        			gTip.transition().delay(1000).style("display","none")
        		}
        		var show = function(){
        			console.debug(rect.attr("height"));
        			gTip.style("display","block");
        			console.debug(rect.attr("height"));
        			rect.transition().duration(500).attr("height",h);
        			rect.transition().delay(500).duration(500).attr("width",w);
        			title.transition().delay(1000).style("display","block")
        			desc.transition().delay(1000).style("display","block")
        		}

        		var wordwrap = function(text, max) {
        			var regex = new RegExp(".{0,"+max+"}(?:\\s|$)","g");
        			var lines = []
        			var line
        			while ((line = regex.exec(text))!="") {
        				lines.push(line);
        			}
        			return lines
        		}


        		this.g = group || that.g.append("g").attr("id","toolTip");
        		this.g.style("display","block")
        			.attr("class", "this")
        			.attr("transform","translate("+this.x+","+this.y+")")
        			.on("click",function(){
        				if(gTip.style("display") == "block"){ hide();}
        				else{ show();}
        				});

        		var circ = this.g.append("circle")
        			.attr("cx",0)
        			.attr("cy",0)
        			.attr("r",r+5)
        			.attr("fill","white")
        			.attr("stroke-width",3)
        			.attr("stroke","steelblue")
        			.attr("stroke-opacity",0.7)
        			.on("mouseover",function(){ d3.select(this).transition().duration(500).attr("r",r+7).attr("stroke-width",7) })
        			.on("mouseout",function(){ d3.select(this).transition().duration(500).attr("r",r+5).attr("stroke-width",3) })

        		var circ2 = this.g.append("circle")
        			.attr("cx",0)
        			.attr("cy",0)
        			.attr("r",r)
        			.attr("fill","white")
        			.attr("stroke-width",2)
        			.attr("stroke","steelblue")
        			.attr("stroke-opacity",0.7)
        			.on("mouseover",function(){ circ.transition().duration(500).attr("r",r+7).attr("stroke-width",7) })
        			.on("mouseout",function(){ circ.transition().duration(500).attr("r",r+5).attr("stroke-width",3) })

        		var ctext = this.g.append("text")
        					.attr("x",-7)
        					.attr("y",7)
        					.style("font-size","18px")
        					.style("fill","steelblue")
        					.style("display","none");

        		var gTip = this.g.append("g").style("display","none").attr("transform","translate(0,0)");

        		var rect = gTip
        		var rect = gTip
        			.append("rect")
        			.attr("rx",6)
        			.attr("ry",6)
        			.attr("x",0)
        			.attr("y",0)
        			.attr("width",10)
        			.attr("height",10)
        			.attr("fill","white")
        			.attr("stroke","steelblue")
        			.attr("stroke-width",2);

        		var title = gTip
        			.append("text")
        			.attr("x",4)
        			.attr("y",20)
        			.attr("transform","translate(5,5)")
        			.style("fill", "steelblue")
        			.style("font-size", "24px")
        			.style("display","none");

        		var desc = gTip
        			.append("text")
        			.attr("x",4)
        			.attr("y",fontSize+14)
        			.attr("transform","translate(5,5)")
        			.style("fill", "black")
        			.style("font-size", fontSize+"px")
        			.style("display","none");

        		this.color = function(color){
        			circ.attr("stroke",color);
        			circ2.attr("stroke",color);
        			rect.attr("stroke",color);
        			return this;
        		}
        		this.title = function(input){
        				title.text(input);
        				return this;
        			}
        		this.ctext  = function(val){
        			ctext.text(val).style("display","block");
        			return this;};
        		this.text = function(input){
        			//title.text(data.TblName);
        			var lines = wordwrap(input, charMax);
        			desc.selectAll("#tspan").remove();
        			for (var i = 0; i < lines.length; i++) {
        				desc.append("tspan").attr("id","tspan").attr("dy",fontSize).attr("x",3).text(lines[i])
        			}
        			if(gTip.style("display")!="none"){
        				h = 45+(lines.length*fontSize);
        				rect.attr("height",h);
        			}

        			return this;
        		};
        		return this;
        	};
        	this.toolTip.prototype = new BaseGraphicsObject();
        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.lineInterpol = function(x1,y1,x2,y2,width,gInput){
        		var l = {};
        		var w = width || 4;
        		var data = [
        			{x:x1,y:y1},
        			{x:x1+40,y:y1},
        			{x:x2-40,y:y2},
        			{x:x2,y:y2}
        		];
        		var g = gInput || that.g.append("g").attr("id","lineInterpol");

        		var x = d3.scale.linear().domain([0,1]).range([0, 1]),
        			y = d3.scale.linear().domain([0,1]).range([0,1]);

        		var line = d3.svg.line()
        			.interpolate("basis") //"step-after")
        			.tension(1000)
        			.x(function(d) { return x(d.x); })
        			.y(function(d) { return y(d.y); });

        		var path = g
        			.append("g")
        			.datum(data)
        			.append("path")
        			.attr("class", "line")
        			.attr("fill","none")
        			.attr("stroke","steelblue")
        			.attr("stroke-width",w)
        			.attr("d", line);

        		this.start  = function(x,y){
        			data[0].x = x;
        			data[1].x = x+40;
        			data[0].y = y;
        			data[1].y = y;
        			path.attr("d", line);
        			return this;};
        		this.end  = function(x,y){
        			data[3].x = x;
        			data[2].x = x-40;
        			data[3].y = y;
        			data[2].y = y;
        			path.attr("d", line);
        			return this;};
        		this.draw  = function(){ path.attr("d", line); return this;};
        		return this
        	}
        	this.lineInterpol.prototype = new BaseGraphicsObject();
        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.plot = function(x,y,w,h){
        		var n = 600; // Data points
        		var t = this;
        		var p = {};
        		p.xp=x; p.yp=y; p.w=w; p.h=h;
        		p.graph = that.g.append("g").attr("transform", "translate(" + p.xp + "," + p.yp + ")");
        		p.graph.append("rect").attr("x",0).attr("y",0)
        				.attr("width",p.w-1)
        				.attr("height",p.h)
        				.attr("stroke","gray")
        				.attr("stroke-opacity",0.6)
        				.attr("fill","black")
        				.attr("fill-opacity",0.05);

        		p.data = [3, 6, 2, 7, 5, 2, 0, 3, 8, 9, 2, 5, 9, 3, 6, 3, 6, 2, 7, 5, 2, 1, 3, 8, 9, 2, 5, 9, 2, 7];
        		this.build = function(){
        			p.line = d3.svg.line().interpolate("cardinal")
        				.x(function(d,i) { return p.x(p.xMap(i)); })
        				.y(function(d) { return p.y(d); });

        			p.xAxis = d3.svg.axis().scale(p.x).tickSize(-p.h).ticks(5).tickSubdivide(true);
        			p.yAxis = d3.svg.axis().scale(p.y).tickSize(-p.w).ticks(4).tickSubdivide(true).orient("left");
        			p.xAxisG = p.graph.append("svg:g")
        				.attr("class", "axis")
        				.attr("transform", "translate(0," + h + ")")
        				.call(p.xAxis);
        			p.yAxisG = p.graph.append("svg:g")
        				.attr("class", "axis")
        				.call(p.yAxis);
        			// console.debug(p.line(p.data));
        			p.linePath = p.graph.append("svg:g").append("svg:path").attr("d", p.line(p.data));

        			if(typeof(p.f2)!='undefined'){
        				console.debug("p.f2");
        				p.linePath2 = p.graph.append("svg:g").append("svg:path").attr("d", p.line(p.data2));
        			};
        			return this;
        		};
        		this.func = function(f){
        			p.f = f;
        			p.updateData();
        			return this;
        			};
        		this.func2 = function(f){
        			p.f2 = f;
        			p.updateData();
        			return this; };
        		this.update = function(){
        			p.updateData();
        			p.linePath.transition().duration(2000).attr("d", p.line(p.data));
        			if(typeof(p.f2)!='undefined'){
        				p.linePath2.transition().duration(2000).attr("d", p.line(p.data2));
        			};
        			return this;
        			}; // push a new line
        		this.zoom = function(){
        			p.zoomend = function(){
        				//t.update();
        			};
        			p.zooming = function(){
        				p.xMin = p.x.invert(0);
        				p.xMax = p.x.invert(p.w);
        				p.xMap.range([p.xMin, p.xMax]);
        				//console.debug();
        				p.xAxisG.call(p.xAxis);
        				p.yAxisG.call(p.yAxis);

        				p.updateData();

        				p.linePath.attr("d", p.line(p.data));
        				if(typeof(p.f2)!='undefined'){
        					p.linePath2.attr("d", p.line(p.data2));
        				};
        			};

        			p.graph.call(d3.behavior.zoom().x(p.x).y(p.y).scaleExtent([0.5, 10])
        				.on("zoom", p.zooming)
        				//.on("zoomend", t.update)
        				);

        			p.cropRect = p.graph.append("clipPath").attr("id","chart-area")
        				.append("rect")
        				.attr("x",0).attr("y",0)
        				.attr("width",p.w)
        				.attr("height",p.h);

        		};
        		p.updateData = function(){
        			p.data = [];
        			for(var i=0;i<n;i++){
        				p.data[i]=p.f(p.xMap(i));
        			};
        			if(typeof(p.f2)!='undefined'){
        				p.data2 = [];
        				for(var i=0;i<n;i++){
        					p.data2[i]=p.f2(p.xMap(i));
        				}
        			};

        		};
        		this.title = function(){};
        		this.labels = function(){};
        		this.range = function(x,y){
        			p.xMin = x[0];
        			p.xMax = x[1];
        			p.xMap = d3.scale.linear().domain([0,n-1]).range([p.xMin, p.xMax]);
        			p.x = d3.scale.linear().domain([x[0], x[1]]).range([0, p.w]);
        			// Y scale will fit values from 0-10 within pixels h-0 (Note the inverted domain for the y-scale: bigger is up!)
        			p.y = d3.scale.linear().domain([y[0], y[1]]).range([p.h, 0]);
        			return this;
        		};


        		// create a simple data array that we'll plot with a line (this array represents only the Y values, X will just be the index location)


        		// X scale will fit all values from data[] within pixels 0-w

        		// automatically determining max range can work something like this
        		// var y = d3.scale.linear().domain([0, d3.max(data)]).range([h, 0]);

        		// create a line function that can convert data[] into x and y points


        		// create yAxis

        		// create left yAxis
        		/*
        		var yAxisLeft = d3.svg.axis().scale(y).ticks(4).orient("left");
        		// Add the y-axis to the left
        		graph.append("svg:g")
        			.attr("class", "y axis")
        			.attr("transform", "translate(-25,0)")
        			.call(yAxisLeft);
        		*/
        		// Add the line by appending an svg:path element with the data line we created above
        		// do this AFTER the axes above so that the line is above the tick-lines

        		return this;
        	}

        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.circleAnimation = function(inner){
        		var interval=0,speed=1, i=0;
        		var g = that.g.append("g").attr("id","animation");
        		var arcA = new that.arc(0,0,inner,0,190,g);
        		var arcB = new that.arc(0,0,inner+3,0,190,g);
        		var arcC = new that.arc(0,0,inner+8,0,190,g) ;

        		arcA.select().attr("stroke-width",2);	arcB.select().attr("stroke-width",3);	arcC.select().attr("stroke-width",4);
        		var initiate = function(){	interval = setInterval(animate, 15);}

        		var animate = function(){
        			arcA.angle(2*i*speed,2*i*speed+180);
        			arcB.angle(-3*i*speed,-3*i*speed+200);
        			arcC.angle(i*speed+40,i*speed+290);
        			i++;
        		}

        		this.move  = function(x,y){ g.attr("transform","translate("+x+","+y+")"); return this; };
        		this.hide  = function(){
        			for(var i=0;i<interval+1;i++){ clearInterval(i) };
        			arcA.unpop();
        			arcB.unpop();
        			arcC.unpop();
        			return this;
        		};
        		this.show  = function(){
        			setTimeout(initiate, 500);
        			arcA.pop();
        			arcB.pop();
        			arcC.pop();
        			return this;
        		};
        		this.speed  = function(input){ speed = input; return this; };
        		this.remove  = function(){};
        		return this
        	}

        	////////////////////////////////////////////////////////////////////////////////////////////////

        	this.slider = function(xVal,yVal,w,min,max){
        		var x=xVal,y=yVal;
        		var obj = "test obj";
        		var g = that.g.append("g").attr("id","slider").attr("transform", "translate(" +x+ ", " +y+ ")");

        		var rectBox = g.append("rect")
                    .attr("rx", 2)
                    .attr("ry", 2)
                    .attr("x", 0)
                    .attr("y", 0)
                    .attr("width", w+30) //d.w) // Function of the number of characters
                    .attr("height", 26)
                    .attr("fill","none")
                    .attr("stroke","steelblue")
                    .attr("stroke-width",2);
        		var gRect = g.append("g").attr("id","gRect");
        		var rect = new that.tip(0,0,"fff",gRect);


        		//var x = d3.scale.linear().domain([min,max]).range([0,w]);
        		var xMap = d3.scale.linear().domain([min,max]).range([0,w]);
        		var xCap = d3.scale.linear().clamp(true).domain([0,w]).range([0,w]);
        		//console.debug("xCap(min)\t"+xCap(min)+"\nxCap(max)\t"+xCap(max))
        		//console.debug("x\t"+y+"\ny\t"+y);
        		var drag = d3.behavior.drag().on("drag", function(){
        			x += d3.event.dx;
        			x = xCap(x);
        			rect.move(x,0).text(Math.round(10*xMap.invert(x))/10);
        		});
        		gRect.call(drag);


        		this.set  = function(start,end){ return this;};
        		this.hide  = function(){ return this;};
        		this.sigfig  = function(){ return this;};
        		this.show  = function(){ return this;};
        		this.drag  = function(){ return this;};
        		this.select = function(){ return obj};
        		this.remove = function(){};
        		return this
        	}

        	this.group = function(id){
        		var g = that.g.append("g").attr("id",id);

        		this.move  = function(x,y){ g.attr("transform", "translate(" +x+ ", " +y+ ")"); return this;};
        		this.hide  = function(){ return this;};

        		this.show  = function(){ return this;};
        		this.drag  = function(){ return this;};
        		this.select  = function(){return g};
        		this.remove  = function(){};
        		return this
        	}


        /*
        this.template = function(x,y){
        		var localVars;
        		var obj = "test obj";
        		var g = that.g.append("g").attr("id","template");

        		this.set  = function(start,end){ return this;};
        		this.move  = function(x,y){ return this;};
        		this.hide  = function(){ return this;};
        		this.attr  = function(){ return this;};
        		this.show  = function(){ return this;};
        		this.drag  = function(){ return this;};
        		this.select  = function(start,end){ console.debug(obj); return obj;};
        		this.remove  = function(){};
        		return this
        	}
        	*/

		this.write = function(txt){ console.debug(txt)}
		//};
    });

