'use strict';


// Declare app level module which depends on filters, and services
angular.module('myApp', [
  'ui.bootstrap',
  'ngRoute',
 // 'myApp.filters',
  'myApp.services',
  'myApp.services-ui',
 // 'myApp.directives',
  'myApp.controllers',
  'myApp.controllers-ui',
  'myApp.controllers.jsEditor'
 // 'myApp.services-editor',
  //'myApp.services-editor.dataService'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/About', {templateUrl: 'views/About.html', controller: 'AboutController'});
  $routeProvider.when('/PlotTool', {templateUrl: 'views/PlotTool.html', controller: 'PlotController'});
  $routeProvider.when('/jsEditor', {templateUrl: 'views/jsEditor.html', controller: 'jsEditorController'});
  $routeProvider.otherwise({redirectTo: 'About'});
}]);
