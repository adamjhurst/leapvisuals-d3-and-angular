'use strict';

/* Controllers */

var ctrlModule = angular.module('myApp.controllers.jsEditor', ['myApp.services'])
	.controller('jsEditorController', ['$scope','dataService', function($scope, dataService) {

        // Remove the backspace event
        $scope.altKey = false;
        $scope.jsError = ""; // Error output
        $scope.selection = {};
        $scope.selection.text = "";
        $scope.codeSnippets  = dataService.getPosts();
        window.test =  $scope.codeSnippets;

        // Update mouse X coordinate
		d3.select("#editor").on("mousemove", function(){
            $scope.mouseX = d3.mouse(this)[0];
            if($scope.altKey==true){
                $scope.injectIntoEditor();
            };
        });

        d3.select(window).on('resize', function(){
            var w = 0.5*window.innerWidth - 20;
            d3.select("svg").attr("width",w);
        });

		// Reset altKey to prevent $scope.injectIntoEditor() after mousemove event

		$scope.keyUp = function($event){
            $scope.altKey = false;
            $scope.selection = {}; // Clear selection
        };

        $scope.keyDown = function($event){
            if($event.altKey){
				$event.preventDefault();
				editor.selection.selectWord();

				if($scope.altKey==false){
					$scope.altKey = true;
					// Initialising the selection
					//$scope.selection.firstLine = editor.getFirstVisibleRow(); // used to reset view after refresh
					$scope.selection.text = editor.session.getTextRange($scope.selection.range);
					$scope.selection.textFloat = parseFloat($scope.selection.text);
					if(isNaN($scope.selection.textFloat)){ return false }
					else{
						$scope.selection.mouseXinitial = $scope.mouseX;
						$scope.selection.range = editor.getSelectionRange();
						$scope.selection.textOriginal = editor.getValue().split("\n");
						$scope.selection.row = $scope.selection.textOriginal[$scope.selection.range.start.row];
						$scope.selection.len = $scope.selection.row.length;
						$scope.selection.tail = $scope.selection.len - $scope.selection.range.end.column;

						$scope.selection.scale = d3.scale.linear().domain([0,$scope.selection.mouseXinitial]).range([0,1]);
					}
				};
				return false;
			} else if($event.keyCode == 8){ 
				// Prevent backspace from going back one page
				$event.preventDefault();
                return false;
			};
		};

		$scope.injectIntoEditor = function(){
            if(isNaN($scope.selection.textFloat)){ return false }; // Return if selection is not a float or integer
            var tempText = $scope.selection.textOriginal; // Load original text for selected row

            // Substitute new integer, calculated as a function of mouse position
            tempText[$scope.selection.range.start.row] =
                            $scope.selection.row.substr(0,$scope.selection.range.start.column)
                            +Math.round($scope.selection.textFloat*$scope.selection.scale($scope.mouseX))
                            +$scope.selection.row.substr($scope.selection.len-$scope.selection.tail); //$scope.selection.range.end.column);

            tempText = tempText.join("\n");

            // Update editor and refresh the D3 visualisation
            editor.setValue(tempText);

            console.debug("first selection "+$scope.selection.firstLine)
            editor.selection.setSelectionRange($scope.selection.range);
            //editor.gotoLine($scope.selection.firstLine);
            $scope.executeScript();
		};


        // Initiate editor
		ace.require("ace/ext/language_tools");
		var editor = ace.edit("editor");
		editor.setTheme("ace/theme/twilight");
		editor.getSession().setMode("ace/mode/javascript");
        window.editor = editor;
		editor.setOptions({
			enableBasicAutocompletion: true,
			enableSnippets: true,
			enableLiveAutocompletion: true
		});

		// Triggered by modal selection
		$scope.$on('codeUpdateEvent', function (event, data) {
			editor.setValue($scope.codeSnippets[data].code);
			editor.clearSelection();
			$scope.executeScript();
		});


		$scope.updateEditor = function(index){

		};

		$scope.executeScript = function(){
			d3.select("svg").remove();
			var js = editor.getValue();
			try{
				eval.call(null,"(function(){var w = 0.46*window.innerWidth,h = 600;"+js+"}())");
				$scope.jsError = "";
				d3.selectAll("svg *").on("mousemove", function(){
					$scope.svgSelection = "SVG Element: "+d3.select(this)[0][0].outerHTML;
					$scope.$apply();
				});

			}
			catch(err){
				var tip = "";
				if(err.message == "undefined is not a function"){
					tip = "Tip: This error is often caused by calling a function that does not exist. Check for typos"
				}
				$scope.jsError = "Error Message:\n"+err+"\n"+tip+"\n" ;
			}
		};
		$scope.executeScript();

		$scope.printSctipt = function(){
			dataService.getPosts()
			//.$promise.then(function(data) {
				//console.debug(data);
			//});
			var js = editor.getValue();
			var codeStore = {
				title:"Code Test",
				category:"Patterns",
				level:"Introduction", // Intermediate, Advanced
				code:js
				};
			console.debug(codeStore);
			console.debug(JSON.stringify(codeStore));
		};

    }]);
// http://www.leapacademy.co.uk/wp-json/posts?filter[category_name]=js-essentials
var ModalDemoCtrl = function ($scope, $modal, $log, dataService) {

  $scope.items = dataService.getPosts();

  $scope.open = function (size) {
    console.debug("open")

    var modalInstance = $modal.open({
      templateUrl: 'myModalContent.html',
      controller: ModalInstanceCtrl,
      size: size,
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });
  };
};

// Please note that $modalInstance represents a modal window (instance) dependency.
// It is not the same as the $modal service used above.

var ModalInstanceCtrl = function ($rootScope, $scope, $modalInstance, items) {

  $scope.items = items;

  $scope.select = function(id){
	$rootScope.$broadcast('codeUpdateEvent',id);
	$scope.ok();
  }

  $scope.selected = {
    item: $scope.items[0]
  };

  $scope.ok = function () {
    $modalInstance.close($scope.selected.item);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
};


function CarouselDemoCtrl($scope) {
  $scope.myInterval = 5000;
}
