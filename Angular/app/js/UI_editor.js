var nodes = new vis.DataSet();
var edges = new vis.DataSet();
var graph = null;
var maxNodeIndex = 6;
var maxEdgeIndex = 6;
var maxLevel = 2;


function draw() {
    //Create Graph
    // randomly create some nodes and edges
    for (var i = 0; i < maxNodeIndex; i++) {
        nodes.add([{
            id: i,
            label: "Blob PPP " + String(i),
            shape: 'box',
            level: maxLevel
        }]);
    }
    nodes.update({id: 0, level: 0, group: 'root'});

    var container = document.getElementById('mygraph');
    var data = {
        nodes: nodes,
        edges: edges
    };

    var options = {
        hierarchicalLayout: {
            nodeSpacing: 500,
            direction: "LR"
        },
        dataManipulation: true,
        onAdd: function(data, callback){
            callback([{
                id: maxNodeIndex,
                label: "Blob PPP " + String(maxNodeIndex),
                shape: 'box',
                level: maxLevel
            }]);
            maxNodeIndex++;
        },
        onConnect: function(data, callback){
            data.id = maxEdgeIndex;
            maxEdgeIndex++;
            callback(data);
        },
        groups: {
            root: {
                // defaults for nodes in this group
                radius: 15,
                color: 'red',
                fontColor: 'white',
                fontSize: 18,
                fontFace: 'courier',
                shape: 'rect'
            }
        }
    };
    graph = new vis.Graph(container, data, options);

    // add event listeners
    graph.on('select', function(params) {
        document.getElementById('selection').innerHTML = 'Selection: ' + params.nodes;
    });

    //Setup Edge Events
    edges.on('add', updateAfterEdgeEvent);
    edges.on('remove', updateAfterEdgeEvent);

    function updateAfterEdgeEvent(event, properties){
        edges.update({id: properties.items[0], style: 'arrow'});

        var cycleNode = cycleDetector();
        if(cycleNode !== false){
            var route = cycleRouteDetector(cycleNode);
            if(route !== false){
                var message = "You have a cycle between the following nodes: ";

                var es = edges.get(route);
                var nodeList = [];

                for(var i =0; i < es.length; ++i){
                    nodeList.push(es[i].from);
                    nodeList.push(es[i].to);
                }

                nodeList = nodeList.filter(function(elem, pos) {
                    return nodeList.indexOf(elem) == pos;
                })

                var ns = nodes.get(nodeList);
                for(var i = 0; i < ns.length; ++i){
                    message = message + ns[i].label;
                    if(i != ns.length - 1){
                        message = message + ", ";
                    }
                }

                message = message + ". Please remove it.";
                alert(message);
                //colorEdges(route);
            }
        }
        var levels = updateLevels(0);
        maxLevel = Math.max.apply(null, levels) + 1;
        logEvent(event, properties);
    }

    edges.add([{
        id: 0,
        from: 0,
        to: 1
    }]);
    edges.add([{
        id: 1,
        from: 0,
        to: 2
    }]);
    edges.add([{
        id: 2,
        from: 1,
        to: 3
    }]);
    edges.add([{
        id: 3,
        from: 1,
        to: 4
    }]);
    edges.add([{
        id: 4,
        from: 2,
        to: 4
    }]);
    edges.add([{
        id: 5,
        from: 2,
        to: 5
    }]);
}

function colorEdges(edgeRoute){
    var color = typeof edgeRoute === 'undefined' ? '#848484' : '#ff0000';

    var childEdges = [];

    if(color === '#848484'){
        childEdges = edges.get();
    } else {
        childEdges = edges.get(edgeRoute);
    }

    for(var i = 0; i < childEdges.length; ++i){
        edges.update({id: childEdges[i].id, 'color.color': color, 'color.highlight': color});
    }
}

function updateLevels(nodeId, depth, newLevels){

    //If we have a cycle exit it
    if(depth > nodes.get().length + 1){
        return newLevels;
    }

    depth = typeof depth !== 'undefined' ? depth : 0;
    newLevels = typeof newLevels !== 'undefined' ? newLevels : new Array();

    logEvent({node: nodeId, depth: depth}, newLevels);

    //Update the node depth, ensure that it is always shown as deep as possible
    var currentLevel = depth;
    if(typeof(newLevels[nodeId]) !== 'undefined'){
        currentLevel = Math.max(newLevels[nodeId], depth);
    }
    nodes.update({id: nodeId, level: currentLevel});
    newLevels[nodeId] = currentLevel;

    //Find Child Nodes
    var childNodes = edges.get({filter: function(item){
        return item.from === nodeId;
    }});

    //Update Child Nodes
    for(var i = 0; i < childNodes.length; ++i){
        newLevels = updateLevels(childNodes[i].to, depth+1, newLevels)
    }
    //logEvent(nodeId, newLevels);
    return newLevels;
}

function cycleDetector(nodeId, depth){
    nodeId = typeof nodeId !== 'undefined' ? nodeId : 0;
    depth = typeof depth !== 'undefined' ? depth : 0;

    if(depth > nodes.get().length + 1){
        logEvent("cycleFound", nodeId);
        return nodeId; //Return the node Id that is in the cycle
    }

    var childNodes = edges.get({filter: function(item){
        return item.from === nodeId;
    }});

    for(var i = 0; i < childNodes.length; ++i){
        return cycleDetector(childNodes[i].to, depth+1)
    }

    return false;
}

function cycleRouteDetector(initialNode, nodeId , route) {
    route = typeof route !== 'undefined' ? route : [];

    if(nodeId === initialNode){
        logEvent("cycleRouteFound", route);
        return route;
    }

    nodeId = typeof nodeId !== 'undefined' ? nodeId : initialNode;

    var childNodes = edges.get({filter: function(item){
        return item.from === nodeId;
    }});

    for(var i = 0; i < childNodes.length; ++i){
        route.push(childNodes[i].id);
        return cycleRouteDetector(initialNode, childNodes[i].to, route)
    }

    return false;
}

function logEvent(event, properties) {
    var log = document.getElementById('log');
    var msg = document.createElement('div');
    msg.innerHTML = 'event=' + JSON.stringify(event) + ', ' +
        'properties=' + JSON.stringify(properties);
    log.firstChild ? log.insertBefore(msg, log.firstChild) : log.appendChild(msg);
}