'use strict';

/* Controllers */

angular.module('myApp.controllers-ui', [])
	.controller('DropdownCtrl', ['$scope',  function($scope){

			  $scope.items = [
				'The first choice!',
				'And another choice for you.',
				'but wait! A third!'
			  ];
			
			
			
			  $scope.status = {
				isopen: false
			  };
			$scope.control_calc_root = function(txt){
				plotService.test();
			}
			  $scope.toggled = function(open) {
				console.log('Dropdown is now: ', open);
			  };

			  $scope.toggleDropdown = function($event) {
				$event.preventDefault();
				$event.stopPropagation();
				$scope.status.isopen = !$scope.status.isopen;
			  };

	}])
	.controller('FunctionCtrl',  ['$scope', function($scope){
		//$scope.
        $scope.functionInput = [
            {name: "f(x) = ",id:"equationString", placeholder:"cos(x)", onEnter:"test()", color: "rgb(1, 52, 64)"},
            {name: "g(x) = ",id:"equationString2", placeholder:"sin(x)", onEnter:"test()", color: "rgb(171, 26, 37)"},
            {name: "k(x) = ",id:"equationString3", placeholder:"0.2cos(3x)", onEnter:"test()", color: "#D97925"},
        ];
		$scope.Dropdown = 1;
		
		$scope.tooltip = [
            {name: "Hide all", onclick: "control.toolTip.hideAll()"},
            {name: "Intersections", onclick: "control.toolTip.intersect()"},
            {name: "Roots", onclick: "control.toolTip.root()"},
            {name: "Max & Min Points", onclick: "control.toolTip.maxmin()"}
        ];
		
        $scope.test = function(){
            console.debug("Hello")
        }
		

	}])
	.controller('DropDownCtrl',['$scope', function($scope){
		$scope.items = [
			'The first choice!',
			'And another choice for you.',
			'but wait! A third!'
		  ];

		  $scope.status = {
			isopen: false
		  };

		  $scope.toggled = function(open) {
			console.log('Dropdown is now: ', open);
		  };

		  $scope.toggleDropdown = function($event) {
			$event.preventDefault();
			$event.stopPropagation();
			$scope.status.isopen = !$scope.status.isopen;
		  };
	}]);