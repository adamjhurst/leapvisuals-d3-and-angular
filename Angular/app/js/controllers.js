'use strict';

/* Controllers */

var ctrlModule = angular.module('myApp.controllers', ['myApp.services'])
	.controller('AboutController', ['$scope', function($scope) {

	  }])
	.controller('PlotController', ['$scope', 'plotService', function($scope, plotService) {
			window.update = plotService.control.parseInput;
			plotService.build();
			$scope.control = plotService.control;

			$scope.parse_input = function(){
				console.debug("test")
				plotService.control.parseInput();
			}
			$scope.cm_hide_all = function(){
				plotService.control.toolTip.hideAll();
			}
			$scope.cm_intersect = function(){
				plotService.control.toolTip.intersect();
			}
			$scope.cm_inflexion = function(){
				plotService.control.toolTip.inflexion();
			}
			$scope.cm_root = function(){
				plotService.control.toolTip.root();
			}
			$scope.cm_maxmin = function(){
				plotService.control.toolTip.maxmin();
			}

			$scope.checkbox = function(txt){
				if(txt == "equationString"){ $scope.control.lineOne.change() }
				else if(txt == "equationString2"){$scope.control.lineTwo.change()}
				else if(txt == "equationString3"){$scope.control.lineThree.change()}
			};
  }]);
