'use strict';

angular.module('myApp.services', [])
	.value('version', '0.1')
	.service('dataService', ['$http', function($http) {

		/*
		var getJSPosts = function(){
			$http({
				method: 'GET',
				url: 'http://www.leapacademy.co.uk/wp-json/posts',
				params: {"filter[category_name]":"js-essentials"
			}})
			.success(function(data, status, headers, config) {
				console.debug(data)
			}).
			error(function(data, status, headers, config) {
			  // called asynchronously if an error occurs
			  // or server returns response with an error status.
			});
		
		};
		*/
		
		function getJSPosts(){
			// This will eventually be a GET request to the server, returning JSON data
			// Use the following API: http://www.leapacademy.co.uk/wp-json/posts?filter[category_name]=js-essentials
			var codeSamples =  [
			    {
					"title":"D3 Fractal",
					"img":"img/CodeFractal.jpg",
					"category":"Patterns",
					"level":"Introduction",
					"code":"var container = d3.select(\"#svgcontainer\")\r\n    .append(\"svg\")\r\n    .attr(\"width\",w)\r\n    .attr(\"height\",h);\r\n\r\nvar color = d3.scale.category20b();\r\nvar f = 10/27;\r\n\r\n// Generating data in the shape of a helix. Each iteration draws one ring. \r\nfor(var k=20;k<125;k++){\r\n    var x = w/2 + 3*k*Math.cos(k*f),\r\n        y = h/2 + 3*k*Math.sin(k*f);\r\n    drawRing(x,y); \r\n}\r\n// draws one ring like object.\r\nfunction drawRing(x,y){\r\n    // Generating the data for one ring of circles, centred on x & y\r\n    var radius = 55, data = [];\r\n    for(var i=0;i<19;i++){\r\n        data.push({\r\n            x:x+radius*Math.sin(i/3),\r\n            y:y+radius*Math.cos(i/3),\r\n            col:color(i),\r\n            opacity:0.2,\r\n            r:40\r\n        });\r\n    }\r\n    // White circle in middle of each ring\r\n    data.push({\r\n        x:x,\r\n        y:y,\r\n        col:\"white\",\r\n        opacity:1,\r\n        r:85\r\n    });\r\n    // Bind data and append a circle for each data point\r\n    container.append(\"g\")\r\n        .selectAll(\"circle\")\r\n        .data(data)\r\n        .enter()\r\n        .append(\"circle\")\r\n        .attr(\"cx\",function(d){return d.x})\r\n        .attr(\"cy\",function(d){return d.y})\r\n        .attr(\"r\",function(d){return d.r})\r\n        .attr(\"fill\",function(d){return d.col})\r\n        .attr(\"fill-opacity\",function(d){return d.opacity});\r\n}"
				},
				{
					"title":"D3 Mousemove Events",
					"img":"img/CodeCirlceEffect.jpg",
					"category":"Patterns",
					"level":"Introduction",
					"code":"var container = d3.select(\"#svgcontainer\")\r\n    .append(\"svg\")\r\n    .attr(\"width\",w)\r\n    .attr(\"height\",h)\r\n    .on(\"mousemove\",update);\r\n\r\nvar i, j, n = 20, data=[];\r\nfor(i=1;i<n;i++){\r\n    for(j=0;j<n;j++){\r\n        data.push({x:w*i/n, y:h*j/n});\r\n    }\r\n}\r\n\r\nvar circles = container.selectAll(\"circle\")\r\n    .data(data)\r\n    .enter()\r\n    .append(\"circle\")\r\n    .attr(\"cx\",function(d){return d.x})\r\n    .attr(\"cy\",function(d){return d.y})\r\n    .attr(\"r\",function(d){return 0.1*length(d.x,w/2, d.y, h/2);}) \r\n    .attr(\"fill\",\"steelblue\")\r\n    .attr(\"fill-opacity\",0.7);\r\n\r\nfunction length(x1,x2,y1,y2){\r\n    return Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2));\r\n}\r\n   \r\nfunction update(){\r\n    var m = d3.mouse(this); // Mouse coordinates\r\n    circles.attr(\"r\",function(d){\r\n        return 0.1*length(d.x,m[0], d.y, m[1]);\r\n    });\r\n}"
				},
				{
					"title":"D3 Transitions",
					"img":"img/CodeSinCurve.jpg", 
					"category":"Patterns",
					"level":"Introduction",
					"code":"var i = 0;\r\n\t\r\nvar container = d3.select(\"#svgcontainer\")\r\n    .append(\"svg\")\r\n    .attr(\"width\",w)\r\n    .attr(\"height\",h)\r\n    .on(\"mousemove\",drawCirc);\r\n    \r\nvar color = d3.scale.category20b();\r\n\r\nfunction drawCirc(){\r\n    // Capture mouse coordinates\r\n    var m = d3.mouse(this);\r\n    \r\n    container.append(\"circle\")\r\n        .attr(\"cx\",function(){return m[0]})\r\n        .attr(\"cy\",function(){return m[1]})\r\n        .attr(\"r\",0)\r\n        .attr(\"fill\",function(){return color(i++)})\r\n        .attr(\"fill-opacity\",0.2)\r\n        .transition() // transition from invisible to radius 30\r\n        .attr(\"r\",30)\r\n        .transition() // transition from radius 30 to invisible\r\n        .duration((5854))\r\n        .attr(\"cx\",m[0]+200*(Math.random()-0.5))\r\n        .attr(\"cy\",m[1]+200*(Math.random()-0.5))\r\n        .attr(\"r\",0) \r\n        .remove();\r\n}\t"
				},
				{
					"title":"Fun with Circles 1",
					"img":"img/CodeDonut.jpg",
					"category":"Patterns",
					"level":"Introduction",
					"code":"var container = d3.select(\"#svgcontainer\")\n    .append(\"svg\")\n    .attr(\"width\",w)\n    .attr(\"height\",h);\n\nvar color = d3.scale.category20b();\n\nfor(var i=0;i<63;i++){\n    container\n        .append(\"circle\")\n        .attr(\"cx\",264+114*Math.sin(i/10))\n        .attr(\"cy\",204+114*Math.cos(i/10))\n        .attr(\"r\",71)\n        .attr(\"fill\",function(){return color(i)})\n        .attr(\"fill-opacity\",0.1);\n}\n\t"
				},
				{
					"title":"Fun with Circles 2",
					"img":"img/CodeSin.jpg",
					"category":"Patterns",
					"level":"Introduction",
					"code":"var container = d3.select(\"#svgcontainer\")\n    .append(\"svg\")\n    .attr(\"width\",w)\n    .attr(\"height\",h);\n\nfor(var i=0;i<85;i++){\n    container\n        .append(\"circle\")\n        .attr(\"cx\",9*i)\n        .attr(\"cy\",190+45*Math.cos(i/3))\n        .attr(\"r\",69)\n        .attr(\"fill\",\"steelblue\")\n        .attr(\"fill-opacity\",0.15);\n}"
				}
			];
			
			return codeSamples;
		}

		this.getPosts = function(){
			// Returns all current code samples.
			return getJSPosts();
		}
	}]);
